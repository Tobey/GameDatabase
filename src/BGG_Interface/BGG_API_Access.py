# Documentation of API: https://boardgamegeek.com/wiki/page/BGG_XML_API2

from xml.etree import ElementTree
from PIL import Image
from io import BytesIO
import re

import requests


def get_search_result(search_string):
    api_string = u"https://www.boardgamegeek.com/xmlapi2/search?query=" + search_string
    response = requests.get(api_string)

    return response


def parse_search_result(search_result, get_full_data):
    tree = ElementTree.fromstring(search_result)
    found_games = []
    for game_meta in tree:
        game_type = game_meta.attrib["type"]
        game_id = game_meta.attrib["id"]
        game_name = game_meta[0].attrib["value"]
        if game_type in ("boardgame", "boardgameexpansion"):
            game = Game(game_name, game_type, game_id)
            if get_full_data:
                game.update()
            found_games.append(game)
            # print(game)
            # print("========================")
    return found_games


class Game(object):

    def __init__(self, name_="", type_="", bgg_id_=0):
        self.name = name_
        self.type = type_
        self.bgg_id = int(bgg_id_)

        self.bgg_url = u"https://boardgamegeek.com/boardgame/"+str(self.bgg_id)

        self.description = ""
        self.year_published = ""

        self.image = ""
        self.thumbnail = ""

        self.min_players = 0
        self.max_players = 99

        self.min_age = 0

        self.play_time_min = 0
        self.play_time_max = 5000

        #ToDo: Parse rating and ranking + storage and loading to database
        self.rating = 0
        self.n_ratings = 0  # Since rating is an average, number of ratings or "bayesaverage" could be used
        self.ranking = 0  # Can be "Not Ranked"

        '''
            Open info: polls (best number of players, best age for players, publisher, category
        '''

    def __str__(self):
        return "Name:\t{}\nType:\t{}\n ID:\t{}".format(self.name, self.type, self.bgg_id)

    def load_from_database(self, raw_game_data):
        [self.name, self.type, self.bgg_id, self.description, self.year_published, self.image, self.thumbnail,
         self.min_players, self.max_players, self.min_age, self.play_time_min, self.play_time_max, self.rating,
         self.n_ratings, self.ranking] = raw_game_data

        self.bgg_url = u"https://boardgamegeek.com/boardgame/" + str(self.bgg_id)

        """
        self.name = raw_game_data[0]
        self.type = raw_game_data[1]
        self.bgg_id = int(raw_game_data[2])
        self.description = raw_game_data[3]
        self.year_published = int(raw_game_data[4])
        self.image = raw_game_data[5]
        self.thumbnail = raw_game_data[6]
        self.min_players = int(raw_game_data[7])
        self.max_players = int(raw_game_data[8])
        self.min_age = int(raw_game_data[9])
        self.play_time_min = int(raw_game_data[10])
        self.play_time_max = int(raw_game_data[11])
        """

    def get_save_string(self):
        attribute_list = [self.name,
                          self.type,
                          self.bgg_id,
                          self.description,
                          self.year_published,
                          self.image,
                          self.thumbnail,
                          self.min_players,
                          self.max_players,
                          self.min_age,
                          self.play_time_min,
                          self.play_time_max,
                          self.rating,
                          self.n_ratings,
                          self.ranking]
        return "\t".join(str(attribute_list))

    def update(self, update_name=False):
        # alternative name to update the info from BGG.
        self.get_game_info(update_name)

    def get_game_info(self, update_name=False):
        # stats=1 adds game statistics such as ratings and rank!
        query = u"https://www.boardgamegeek.com/xmlapi2/thing?id=" + str(self.bgg_id) + "&stats=1"
        raw_data = requests.get(query)
        xml_data = ElementTree.fromstring(raw_data.content)

        game_data = xml_data[0]

        if update_name:
            self.set_name_from_xml(game_data)
            self.set_type_form_xml(game_data)

        # ToDo: Replace unicode to readable strings.
        self.set_description_from_xml(game_data)
        self.set_year_published_from_xml(game_data)

        self.set_image_from_xml(game_data)
        self.set_thumbnail_from_xml(game_data)

        self.set_min_players_from_xml(game_data)
        self.set_max_players_from_xml(game_data)

        self.set_play_time_min_from_xml(game_data)
        self.set_play_time_max_from_xml(game_data)

        self.set_min_age_from_xml(game_data)

        self.set_rating_from_xml(game_data)
        self.set_ranking_from_xml(game_data)

    def set_name_from_xml(self, game_data):
        if game_data.find("name") is not None:
            self.name = game_data.find("name").attrib["value"]

    def set_type_form_xml(self, game_data):
        try:
            self.type = game_data.attrib["type"]
        except:
            print(self.bgg_id)
            self.type = "unknown"

    def set_rating_from_xml(self, game_data):
        if game_data.find("statistics") is not None:
            self.rating = float(game_data.find("statistics").find("ratings").find("average").attrib["value"])
            self.n_ratings = int(game_data.find("statistics").find("ratings").find("usersrated").attrib["value"])

    def set_ranking_from_xml(self, game_data):
        if game_data.find("statistics") is not None:
            ranking_string = game_data.find("statistics").find("ratings").find("ranks").find("rank").attrib["value"]
            if ranking_string == 'Not Ranked':
                self.ranking = 999999
            else:
                self.ranking = int(ranking_string)
        else:
            self.ranking = 999999

    def set_min_age_from_xml(self, game_data):
        if game_data.find("minage") is not None:
            self.min_age = int(game_data.find("minage").attrib["value"])

    def show_image(self):
        response = requests.get(self.image)
        Image.open(BytesIO(response.content)).show()

    def show_thumbnail(self):
        response = requests.get(self.thumbnail)
        Image.open(BytesIO(response.content)).show()

    def set_play_time_max_from_xml(self, game_data):
        if game_data.find("maxplaytime") is not None:
            self.play_time_max = int(game_data.find("maxplaytime").attrib["value"])

    def set_play_time_min_from_xml(self, game_data):
        if game_data.find("minplaytime") is not None:
            self.play_time_min = int(game_data.find("minplaytime").attrib["value"])

    def set_max_players_from_xml(self, game_data):
        if game_data.find("maxplayers") is not None:
            self.max_players = int(game_data.find("maxplayers").attrib["value"])

    def set_min_players_from_xml(self, game_data):
        if game_data.find("minplayers") is not None:
            self.min_players = int(game_data.find("minplayers").attrib["value"])

    def set_thumbnail_from_xml(self, game_data):
        if game_data.find("thumbnail") is not None:
            self.thumbnail = game_data.find("thumbnail").text

    def set_image_from_xml(self, game_data):
        if game_data.find("image") is not None:
            self.image = game_data.find("image").text

    def set_year_published_from_xml(self, game_data):
        if game_data.find("yearpublished") is not None:
            self.year_published = int(game_data.find("yearpublished").attrib["value"])

    def set_description_from_xml(self, game_data):
        if game_data.find("description") is not None:
            self.description = game_data.find("description").text.replace(r"&#10;", "\n").replace(r"&amp;", "&").replace(r"&quot;", "\"")
