# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\GameDataBase.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.

# Created using pyuic5 –x "filename".ui –o "filename".py
import os

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QDialog, QActionGroup, QAction
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication

import GameFilter
from GameDataBaseGui_new import Ui_MainWindow
from Filter import Ui_filter_dialog
import Database.Sqlite as SQL
import requests
from BGG_Interface.BGG_API_Access import get_search_result, parse_search_result, Game

class FilterDialog(Ui_filter_dialog):
    def __init__(self, filter_dialog):
        super(FilterDialog, self).__init__()
        self.setupUi(filter_dialog)
        self.sort_by_combo_box.addItem("Alphabetical", GameFilter.GameSort.ALPHABETICAL)
        self.sort_by_combo_box.addItem("BGG Rating", GameFilter.GameSort.RATING)
        self.sort_by_combo_box.addItem("BGG Ranking", GameFilter.GameSort.RANKING)
        self.sort_by_combo_box.addItem("Game Duration", GameFilter.GameSort.DURATION)
        self.sort_by_combo_box.addItem("Min Age", GameFilter.GameSort.AGE)
        self.sort_by_combo_box.addItem("Min Players", GameFilter.GameSort.MIN_PLAYER)
        self.sort_by_combo_box.setCurrentIndex(0)

    def set_values(self, game_filter):
        self.min_rating_slider.setValue(game_filter.min_rating)
        self.min_rating_value_label.setText(str(game_filter.min_rating))
        self.min_player_input.setValue(game_filter.min_player)
        self.max_player_input.setValue(game_filter.max_player)
        self.min_age_input.setValue(game_filter.min_age)
        self.show_expansions.setChecked(game_filter.allow_expansions)
        self.max_duration_slider.setValue(game_filter.max_duration)
        self.max_duration_value_label.setNum(game_filter.max_duration)
        self.sort_by_combo_box.setCurrentIndex(int(game_filter.sort_by))

    def get_values(self):
        values = {
            "min_player": int(self.min_player_input.text()),
            "max_player": int(self.max_player_input.text()),
            "min_rating": self.min_rating_slider.value(),
            "min_age": int(self.min_age_input.text()),
            "max_duration": self.max_duration_slider.value(),
            "sort_by": self.sort_by_combo_box.currentData(),
            "show_expansions": self.show_expansions.isChecked()
        }
        return values


class BggGui(Ui_MainWindow):

    def __init__(self):
        self.database_name = "MyGames"
        self.wishlist_name = "Wishlist"
        self.database_folder = "/Data"
        self.con_owned = SQL.create_database(self.database_name, self.database_folder)
        SQL.create_game_table(self.con_owned)
        self.con_wish = SQL.create_database(self.wishlist_name, self.database_folder)
        SQL.create_game_table(self.con_wish)
        self.con = self.con_owned
        self.con_memory = SQL.create_memory_database()
        self.is_database_mode = False
        self.game_filter = GameFilter.GameFilter()

    def setupUi(self, MainWindow):
        super().setupUi(MainWindow)
        self.menuData_Base.addSection("Used Database")

        self.action_use_bgg_database = QAction(MainWindow)
        self.action_use_bgg_database.setText("BGG")
        self.action_use_bgg_database.setCheckable(True)
        self.action_use_bgg_database.setChecked(True)
        self.menuData_Base.addAction(self.action_use_bgg_database)

        self.action_use_owned_games_database = QAction(MainWindow)
        self.action_use_owned_games_database.setText("My Games")
        self.action_use_owned_games_database.setCheckable(True)
        self.action_use_owned_games_database.setChecked(False)
        self.menuData_Base.addAction(self.action_use_owned_games_database)

        self.action_use_wishlist_database = QAction(MainWindow)
        self.action_use_wishlist_database.setText("My Wishlist")
        self.action_use_wishlist_database.setCheckable(True)
        self.action_use_wishlist_database.setChecked(False)
        self.menuData_Base.addAction(self.action_use_wishlist_database)

        self.selected_database = QActionGroup(MainWindow)
        self.selected_database.setExclusionPolicy(QActionGroup.ExclusionPolicy.Exclusive)
        self.selected_database.addAction(self.action_use_bgg_database)
        self.selected_database.addAction(self.action_use_owned_games_database)
        self.selected_database.addAction(self.action_use_wishlist_database)

    def use_owned_games(self):
        self.set_database_mode()
        self.con = self.con_owned
        if self.is_database_mode:
            self.load_database()

    def use_wishlist(self):
        self.set_database_mode()
        self.con = self.con_wish
        if self.is_database_mode:
            self.load_database()

    def create_connections(self):
        self.search_bar.returnPressed.connect(self.search_game)
        # clears the searchbar on click. Problem: Repositioning the cursor with mouse not possible...
        # Disabled due to issue above. Could be changed in the future.
        # If activated, the QLineEdit search_bar needs to be a ClickableLineEdit. Will be overwritten by QtDesigner on
        # automated code creation.
        # self.search_bar.clicked.connect(self.search_bar.clear)
        self.search_button.clicked.connect(self.search_game)
        self.search_results_combo.currentIndexChanged.connect(self.show_game)
        self.actionLoad_from_database.triggered.connect(self.load_database)
        self.actionRemove_current_game_from_database.triggered.connect(self.delete_game_from_database)
        self.actionAdd_current_game_to_database.triggered.connect(self.add_game_to_database)
        self.database_action_button.clicked.connect(self.add_game_to_database)
        self.database_button_add_to_wishlist.clicked.connect(self.add_game_to_wishlist)
        self.actionAdd_current_game_to_database.setDisabled(False)
        self.actionRemove_current_game_from_database.setDisabled(True)
        self.actionUpdate_data_base_info_from_BGG.triggered.connect(self.update_database)
        self.actionSet_Filter.triggered.connect(self.open_filter_dialog)
        self.actionBackup_Database.triggered.connect(self.backup_database)
        self.preview_picture.mousePressEvent = self.open_bgg_url
        self.action_use_wishlist_database.triggered.connect(self.use_wishlist)
        self.action_use_owned_games_database.triggered.connect(self.use_owned_games)
        self.action_use_bgg_database.triggered.connect(self.set_search_mode)


    def open_filter_dialog(self):
        dlg = QDialog(self.centralwidget)
        filter_dialog = FilterDialog(dlg)
        filter_dialog.set_values(self.game_filter)
        dlg.show()
        if dlg.exec_() and dlg.result() == QDialog.Accepted:
            values = filter_dialog.get_values()
            self.game_filter.set_from_dict(values)
            self.actionApply_filter.setChecked(True)

    def update_database(self):
        if self.is_database_mode:
            game_infos = []
            for entry in range(self.search_results_combo.count()):
                game_infos.append((self.search_results_combo.itemData(entry).name,
                                  self.search_results_combo.itemData(entry).type,
                                  self.search_results_combo.itemData(entry).bgg_id))

        for game_info in game_infos:
            game = Game(game_info[0], game_info[1], game_info[2])
            game.update()
            SQL.delete_game(self.con, game.bgg_id)
            SQL.add_game(self.con, game)

    def search_game(self):
        search_string = self.search_bar.text()
        if self.is_database_mode:
            raw_games = SQL.search_game(self.con, search_string)
            list_of_games = []
            for raw_game in raw_games:
                game = Game()
                game.load_from_database(raw_game)
                list_of_games.append(game)
            list_of_games = self.sort_games(list_of_games)
            self.search_results_combo.clear()
            for x in list_of_games:
                if (not self.actionApply_filter.isChecked()) or self.game_filter.is_filtered(x):
                    self.search_results_combo.addItem(x.name, x)
        else:
            self.set_search_mode()
            response = get_search_result(search_string)
            QApplication.setOverrideCursor(Qt.WaitCursor)
            search_results = parse_search_result(response.content, self.actionApply_filter.isChecked())
            search_results = self.sort_games(search_results)
            self.search_results_combo.clear()
            for x in search_results:
                if (not self.actionApply_filter.isChecked()) or self.game_filter.is_filtered(x):
                    self.search_results_combo.addItem(x.name, x)
            QApplication.restoreOverrideCursor()
        if self.search_results_combo.count() == 0:
            self.clear_game()
            self.description_text_browser.setText("No games found")

    def sort_games(self, search_results):
        if self.game_filter.sort_by == GameFilter.GameSort.ALPHABETICAL:
            search_results.sort(key=lambda x: x.name)
        elif self.game_filter.sort_by == GameFilter.GameSort.AGE:
            search_results.sort(key=lambda x: x.min_age)
        elif self.game_filter.sort_by == GameFilter.GameSort.RATING:
            search_results.sort(key=lambda x: x.rating, reverse=True)
        elif self.game_filter.sort_by == GameFilter.GameSort.RANKING:
            search_results.sort(key=lambda x: x.ranking)
        elif self.game_filter.sort_by == GameFilter.GameSort.DURATION:
            search_results.sort(key=lambda x: x.play_time_max)
        elif self.game_filter.sort_by == GameFilter.GameSort.MIN_PLAYER:
            search_results.sort(key=lambda x: x.min_players)
        return search_results

    def clear_game(self):
        self.description_text_browser.setText("No game selected")
        self.preview_picture.clear()
        self.rating_value_label.clear()
        self.rank_value_label.clear()
        self.num_player_value_label.clear()
        self.age_value_label.clear()
        self.play_time_value_label.clear()

    def set_search_mode(self):
        self.is_database_mode = False
        self.actionAdd_current_game_to_database.setDisabled(False)
        self.actionRemove_current_game_from_database.setDisabled(True)
        self.database_action_button.disconnect()
        self.database_action_button.clicked.connect(self.add_game_to_database)
        self.database_action_button.setText("Add to database")

    def set_database_mode(self):
        self.is_database_mode = True
        self.search_bar.setText("Loaded from data base")
        self.actionAdd_current_game_to_database.setDisabled(True)
        self.actionRemove_current_game_from_database.setDisabled(False)
        self.database_action_button.disconnect()
        self.database_action_button.clicked.connect(self.delete_game_from_database)
        self.database_action_button.setText("Remove from database")

    def load_database(self):
        self.set_database_mode()

        self.search_results_combo.clear()
        raw_games = SQL.get_all_game(self.con)
        list_of_games = []
        for raw_game in raw_games:
            game = Game()
            game.load_from_database(raw_game)
            list_of_games.append(game)
        list_of_games = self.sort_games(list_of_games)
        for x in list_of_games:
            if (not self.actionApply_filter.isChecked()) or self.game_filter.is_filtered(x):
                self.search_results_combo.addItem(x.name, x)
        if self.search_results_combo.count() == 0:
            self.clear_game()

    def backup_database(self):
        SQL.backup_database(self.database_name, self.database_folder)

    def delete_game_from_database(self):
        bgg_id = self.search_results_combo.currentData().bgg_id
        ret = QMessageBox.question(self.centralwidget, 'Confirm deletion', "Do you really want to delete \"{}\" from the database?".format(self.search_results_combo.currentData().name), QMessageBox.Yes | QMessageBox.No)
        if ret == QMessageBox.Yes:
            SQL.delete_game(self.con, bgg_id)
            self.load_database()

    def add_game_to_database(self):
        success = SQL.add_game(self.con_owned, self.search_results_combo.currentData())
        if not success:
            message_box = QMessageBox()
            message_box.setText("Game {} is already in database and could not be added".format(self.search_results_combo.currentData().name))
            message_box.setWindowTitle("Could not add game")
            message_box.exec()

    def add_game_to_wishlist(self):
        success = SQL.add_game(self.con_wish, self.search_results_combo.currentData())
        if not success:
            message_box = QMessageBox()
            message_box.setText("Game {} is already in database and could not be added".format(self.search_results_combo.currentData().name))
            message_box.setWindowTitle("Could not add game")
            message_box.exec()

    def open_bgg_url(self, event):
        game = self.search_results_combo.currentData()
        if not game:
            return
        if not self.is_database_mode:
            os.startfile(game.bgg_url)

    def show_game(self):
        game = self.search_results_combo.currentData()
        if not game:
            return
        if not self.is_database_mode:
            game.update()
        self.description_text_browser.setText(game.description)

        if game.image != "":
            response = requests.get(game.image)
            image = QtGui.QImage()
            image.loadFromData(response.content)
            pm = QtGui.QPixmap(image)
        else:
            pm = QtGui.QPixmap(r".\Data\empty_thumbnail_dice.jpg")

        w = self.preview_picture.width()
        h = self.preview_picture.height()
        self.preview_picture.setPixmap(pm.scaled(w, h, transformMode=QtCore.Qt.SmoothTransformation))

        self.rating_value_label.setText("{:.1f} ({})".format(game.rating, game.n_ratings))
        self.rank_value_label.setText("{}".format(game.ranking))
        self.num_player_value_label.setText("{} - {}".format(game.min_players, game.max_players))
        self.age_value_label.setText("{}+".format(game.min_age))
        if game.play_time_min == game.play_time_max:
            self.play_time_value_label.setText("{} min".format(game.play_time_min))
        else:
            self.play_time_value_label.setText("{} - {} min".format(game.play_time_min, game.play_time_max))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = BggGui()
    ui.setupUi(MainWindow)
    MainWindow.show()
    ui.create_connections()





    '''
    response = get_search_result("Brass")

    [game.update() for game in search_results]
    results = []

    [results.append(x.name) for x in search_results]
    print(results)
    ui.search_results_combo.addItems(results)
    ui.description.setText(search_results[6].description)

    response = requests.get(search_results[6].image)
    image = QtGui.QImage()
    image.loadFromData(response.content)
    w = ui.preview_picture.width()
    h = ui.preview_picture.height()
    pm = QtGui.QPixmap(image)

    ui.preview_picture.setPixmap(pm.scaled(w, h))
    '''

    sys.exit(app.exec_())
