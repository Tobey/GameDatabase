import urllib.request
import re
import Database.Sqlite
from datetime import datetime
from BGG_Interface.BGG_API_Access import Game

#Idee: Benutze letzte existierende Datenbank. Update bestehende Spiele und füge neue Top100 dazu.

urllib.request.urlretrieve(r"https://boardgamegeek.com/browse/boardgame", filename=r"bgg100.txt")

bgg_100 = open(r"bgg100.txt")
#print(bgg_100.read())

regex_bggID = "<a\s*href=\"/boardgame/(\d*)/"

# find matches
m = re.findall(regex_bggID, bgg_100.read())
#convert to ints
ids = results = list(map(int, m))
# make unique
print(list(set(ids)))
now = datetime.now()
#games = []
con = Database.Sqlite.create_database("{}_{}.dbb".format(now.strftime("%Y-%m-%d__%H_%M_%S"), "bgg_100"))
Database.Sqlite.create_game_table(con)
for game_id in list(set(ids)):
    #print(game_id)
    game = Game(bgg_id_=game_id)
    game.update(True)
    #games.append(game)

    Database.Sqlite.add_game(con, game)



