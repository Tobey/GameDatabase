from enum import IntEnum, auto


class GameFilter:
    def __init__(self):
        # Values here assigned only to comply to PEP8, values set in reset.
        self.min_player = 0
        self.max_player = 0
        self.min_rating = 0
        self.min_age = 99
        self.max_duration = 9999
        self.sort_by = GameSort.ALPHABETICAL
        self.allow_expansions = True
        self.reset()

    def reset(self):
        self.min_player = 0
        self.max_player = 0
        self.min_rating = 0
        self.min_age = 99
        self.max_duration = 9999
        self.allow_expansions = True
        self.sort_by = GameSort.ALPHABETICAL

    def set_from_dict(self, filter_dict):
        self.min_player = filter_dict["min_player"]
        self.max_player = filter_dict["max_player"]
        self.min_rating = filter_dict["min_rating"]
        self.min_age = filter_dict["min_age"]
        # GUI can select up to 120min. Should be 120 or more in this case
        if filter_dict["max_duration"] == 120:
            self.max_duration = 9999999
        else:
            self.max_duration = filter_dict["max_duration"]
        self.sort_by = filter_dict["sort_by"]
        self.allow_expansions = filter_dict["show_expansions"]

    def is_filtered(self, game):
        # decides whether a game passes the filter
        min_players_passed = game.min_players >= self.min_player
        max_players_passed = game.max_players >= self.max_player
        min_rating_passed = game.rating >= self.min_rating
        age_passed = game.min_age <= self.min_age
        duration_passed = game.play_time_max < self.max_duration
        expansion_passed = self.allow_expansions or game.type == "boardgame"
        if min_players_passed and \
                max_players_passed and \
                min_rating_passed and \
                age_passed and \
                duration_passed and \
                expansion_passed:
            return True
        else:
            return False


class GameSort(IntEnum):
    ALPHABETICAL = 0
    RATING = 1
    DURATION = 2
    AGE = 3
    MIN_PLAYER = 4
    RANKING = 5
