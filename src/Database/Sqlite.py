import sqlite3
from shutil import copyfile
from datetime import datetime
import os


def restore_dir(function):
    # decorator to restore original working directory
    def inner_function(*args, **kwargs):
        working_directory = os.getcwd()
        result = function(*args, **kwargs)
        os.chdir(working_directory)
        return result
    return inner_function


@restore_dir
def create_database(name, folder="."):
    os.chdir(os.curdir+folder)
    con = sqlite3.connect(name+'.db')
    return con


def create_memory_database():
    return sqlite3.connect(":memory:")


@restore_dir
def backup_database(name, folder="."):
    os.chdir(os.curdir + folder)
    now = datetime.now()
    copyfile(name+".db", "{}_{}.dbb".format(now.strftime("%Y-%m-%d__%H_%M_%S"), name))


def create_game_table(con):
    cur = con.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS games 
                (name TEXT, 
                type TEXT, 
                bgg_id INTEGER UNIQUE, 
                description TEXT, 
                year_published INTEGER, 
                image TEXT, 
                thumbnail TEXT, 
                min_players INTEGER, 
                max_players INTEGER, 
                min_age INTEGER, 
                playtime_min INTEGER, 
                playtime_max INTEGER,
                rating REAL,
                n_ratings INTEGER,
                ranking INTEGER)''')


def add_game(con, game):
    cur = con.cursor()
    query = "SELECT * FROM games WHERE bgg_id=?"
    formats = (game.bgg_id, )
    cur.execute(query, formats)
    if not cur.fetchall():
        cur.execute("insert into games values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    (game.name,
                     game.type,
                     game.bgg_id,
                     game.description,
                     game.year_published,
                     game.image,
                     game.thumbnail,
                     game.min_players,
                     game.max_players,
                     game.min_age,
                     game.play_time_min,
                     game.play_time_max,
                     game.rating,
                     game.n_ratings,
                     game.ranking))
        con.commit()
        return True
    else:
        return False


def delete_game(con, bgg_id):
    cur = con.cursor()
    query = "DELETE FROM games WHERE bgg_id=?"
    formats = (bgg_id, )
    cur.execute(query, formats)
    con.commit()


def get_all_game(con):
    cur = con.cursor()
    cur.execute("SELECT * FROM games ORDER BY name")
    return cur.fetchall()


def search_game(con, search_string):
    cur = con.cursor()
    query = "SELECT * FROM games WHERE name LIKE '%{}%'".format(search_string)
    cur.execute(query)
    return cur.fetchall()


def close_data_base(con):
    con.close()
